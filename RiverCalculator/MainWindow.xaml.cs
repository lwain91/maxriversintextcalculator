﻿// River Calculator Program
//
// By Lewis Wain, Ishan Pandya, Tony Kwak, Ajit Oberoi

using Microsoft.Win32;
using System;
using System.Windows;

namespace RiverCalculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        VM vm = new VM();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }

        private void LoadFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Text file (*.txt)|*.txt";
            open.DefaultExt = ".txt";
            // Show open file dialog box
            Nullable<bool> result = open.ShowDialog();
            // Process open file dialog box results
            if (result == true)
            {
                if (vm.GetFileData(open.FileName) == false)
                    MessageBox.Show("There was an error processing the file! Please ensure the file has an integer number" +
                        " at the beginning which describes the word count accurately, that no word exceed 80 characters " +
                        " and that only uppercase and lowercase letters are used for words.  You may only have one " +
                        "space between words and the word count must be at least two and at most 2500.", "Input Error!");
            }
        }

        private void Calc_Click(object sender, RoutedEventArgs e)
        {
            vm.CalculateMaxRiver();
        }
    }
}
