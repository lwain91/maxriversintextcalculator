﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;


namespace RiverCalculator
{
    public class VM : INotifyPropertyChanged
    {
        #region Constants
        private const int MAX_WORD_LENGTH = 80;
        private const int MINIMUM_WORDS = 2;
        private const int MAXIMUM_WORDS = 2500;

        #endregion

        #region Class variables
        private int initialLineWidth = 0;
        #endregion

        #region Properties
        private string currentFile = "None";
        public string CurrentFile
        {
            get => currentFile;
            set { currentFile = value; changed(); }
        }

        private string outputText = "Please click the load button and choose " + Environment.NewLine +
            "a .txt file to see the output.";
        public string OutputText
        {
            get => outputText;
            set { outputText = value; changed(); }
        }

        private string calculateVisibility = "Hidden";
        public string CalculateVisibility
        {
            get => calculateVisibility;
            set { calculateVisibility = value; changed(); }
        }

        private Page layout = new Page();
        public Page Layout
        {
            get => layout;
            set { layout = value; changed(); }
        }
        #endregion

        #region Functions
        public bool GetFileData(string fileName)
        {
            List<string> words = new List<string>();
            bool noIssues = true;
            int wordCount = 0;
            string reformedText = "";
            string str = File.ReadAllText(fileName);
            try
            {
                // Validate text can be split and first array element is a valid integer.
                string[] tmp = str.Split(' ');
                layout.WordCount = Convert.ToInt32(tmp[0]);
                tmp = tmp.Skip(1).ToArray();
                foreach (string s in tmp)
                    words.Add(s);
            }
            catch
            {
                noIssues = false;
            }
            if (noIssues)
            {
                foreach (string s in words)
                    reformedText += s + " ";
                // Avoid index out of range in case of no words.
                if (words.Count - 1 >= 0)
                    words[words.Count - 1] = words[words.Count - 1].Trim();
                else
                    noIssues = false;                
            }
            if (noIssues)
            {
                // Text must contain only spaces, letters or periods.
                if (!reformedText.All(c => IsSpace(c) || char.IsLetter(c) || IsPeriod(c)))
                    noIssues = false;
                foreach (string s in words)
                {
                    wordCount++;
                    // If words are not only letters or periods or max word length is exceeded raise an issue.
                    if (!s.All(c => char.IsLetter(c) || IsPeriod(c)) || s.Length > MAX_WORD_LENGTH)
                        noIssues = false;
                }
                // Word count must match what is specified in the .txt file and the actual count of words done here.
                if (wordCount != layout.WordCount || (wordCount < MINIMUM_WORDS && wordCount > MAXIMUM_WORDS))
                    noIssues = false;
            }
            if (noIssues)
            {
                CurrentFile = fileName;
                OutputText = "The file is approved. Click " + Environment.NewLine +
                    "calculate to determine maximum " + Environment.NewLine +
                    "river size.";
                CalculateVisibility = "Visible";
                layout.RawText = words;
                layout.LongestRiver = 0;
                layout.LineWidth = 0;
                initialLineWidth = 0;                
                foreach (string s in words)
                {
                    // Get maximum size of word which will determine minimum linewidth.
                    if (s.Length >= initialLineWidth)
                        initialLineWidth = s.Length;
                }
                // Call changed() to update properties.
                changed("Layout");
            }
            return noIssues;
        }

        public bool IsPeriod(char c) => c == '.' ? true : false;

        public bool IsSpace(char c) => c == ' ' ? true : false;

        public void CalculateMaxRiver()
        {
            // Initialize variables
            layout.LineWidth = initialLineWidth;
            OutputText = "Calculating maximum river...";
            // lineStructure is used to determine maximum river size.
            List<string> lineStructure = new List<string>();
            // We initialize lineStructure to be a list of strings with maximum element length equal to maximum word
            // size.  We calculated maximum word size in the GetFileData() function.
            lineStructure = CalculateLinesStructure(layout.RawText, layout.LineWidth);
            // Keep track of the maximum river sizes for each "Document".
            List<int> maxRiverSizes = new List<int>();
            // This list binds max. river length to line width and is queried later on.
            List<LayoutData> layouts = new List<LayoutData>();
            int iterationNum = 0;
            // Now we are waiting for lineStructure to be a list of strings of size 1.
            // We determine the maximum river size for each Count of line structure
            while (lineStructure.Count > 1)
            {
                // Get the "Document" maximum river
                int maxRiver = CalculateMaxRiver(lineStructure);
                // Track the iteration ID
                iterationNum++;
                // Add to List of Max Rivers
                maxRiverSizes.Add(maxRiver);
                // Add to LayoutData list:
                layouts.Add(new LayoutData() 
                { IterationID = iterationNum, LineWidth = layout.LineWidth, MaxRiver = maxRiver });
                // Increment the lineWidth and recalculate lineStructure.
                layout.LineWidth++;
                lineStructure = new List<string>(CalculateLinesStructure(layout.RawText, layout.LineWidth));
            }
            // Outside while loop report maximum river length from List:
            int listMaxRiver = 0;
            foreach (int i in maxRiverSizes)
            {
                if (i > listMaxRiver)
                    listMaxRiver = i;
            }
            // Query the List<LayoutData> for maximum river data.
            var test = layouts.Where(l => l.MaxRiver == listMaxRiver)
                                        .OrderBy(p => p.LineWidth)
                                        .Select(p => new { p.IterationID, p.LineWidth, p.MaxRiver }); 
            // Get the lowest-line-width LayoutData datum that also has a maximum river size (the first element)
            LayoutData results = new LayoutData();
            foreach (var p in test)
            {
                results.LineWidth = p.LineWidth;
                results.MaxRiver = p.MaxRiver;
                break;
            }
            // Set the view variables:
            layout.LongestRiver = results.MaxRiver;
            layout.LineWidth = results.LineWidth;
            
            if (results.MaxRiver == 0) // Handle max river = 0 case (2 words):
            {
                OutputText = $"{layout.RawText[0] + " " + layout.RawText[1]}";
                layout.LineWidth = layout.RawText[0].Length + 1 + layout.RawText[1].Length;
                layout.LongestRiver = 1;
            }
            else
                DisplayText(layout.RawText, results.LineWidth);
            // Update the view            
            changed("Layout");
        }

        public void DisplayText(List<string> rawText, int lineWidth)
        {
            OutputText = "";
            List<string> displayText = new List<string>(CalculateLinesStructure(rawText, lineWidth));
            foreach (string s in displayText)
            {
                OutputText += s + Environment.NewLine;
            }
        }

        public List<string> CalculateLinesStructure(List<string> words, int lineWidth)
        {
            // Copy the input list of strings to a new list because it is a reference variable that is used again
            // and this function removes words from the list.
            List<string> input = new List<string>();
            foreach (string str in words)
            {
                input.Add(str);
            }
            // lines is output that hasn't been trimmed:
            List<string> lines = new List<string>();  
            // trimmedLines is output that has been trimmed and will be returned by this function:
            List<string> trimmedLines = new List<string>();
            // initialize variables and begin iterations:
            bool firstWordOnLine = true;
            bool addMoreToLine = false;
            int lineIndex = 0;
            while (input.Any())
            {
                if (firstWordOnLine)
                {
                    lines.Add(input[0]); // Add the first word to a to a new line.
                    if (lineWidth - lines[lineIndex].Length >= 2) // If you can fit a space, add one.
                        lines[lineIndex] += " ";                    
                    input.RemoveAt(0); // Do not process the word again.
                }
                firstWordOnLine = false;
                if (addMoreToLine)
                {
                    lines[lineIndex] += input[0];
                    if (lineWidth - lines[lineIndex].Length >= 2)
                        lines[lineIndex] += " ";                    
                    input.RemoveAt(0); // Do not process the word again.
                }
                // Calculate the current line length.
                int lineLength = lines[lineIndex].Length;
                
                if (input.Any()) // If not all words have been processed.
                {
                    if (lineWidth >= lineLength + input[0].Length) // If room for another word.
                    {
                        addMoreToLine = true;
                    }
                    else // Start a new line.
                    {
                        addMoreToLine = false;
                        firstWordOnLine = true;
                        lineIndex++;
                    }
                }
            }
            // End while loop
            // Trim and return the document:
            foreach (string l in lines)
            {
                trimmedLines.Add(l.Trim());
            }            
            return trimmedLines;
        }

        public int CalculateMaxRiver(List<string> lineStructure)
        {
            // Generate a map (List of List<int>) of 1's and 0's representing spaces and non-spaces respectively.
            // This will be used to determine where a river is extended. 
            // The basic idea is that if mapOfSpaces[x][y] + mapOfSpaces[i][j] = 2 
            // Then the river will be one unit longer at mapOfRivers[i][j] (provided i and j represent a space on the
            // line below and x and y represent a space on the line above).            
            List<List<int>> mapOfSpaces = new List<List<int>>();
            foreach (string s in lineStructure)
            {
                s.Trim();
                List<int> charsToNumbers = new List<int>();
                foreach (char c in s)
                {
                    if (c == ' ')
                        charsToNumbers.Add(1);
                    else
                        charsToNumbers.Add(0);
                }
                mapOfSpaces.Add(charsToNumbers);
            }
            // Now that mapOfSpaces is populated with 0's and 1's, we populate a mapOfRivers with the same values.            
            List<List<int>> mapOfRivers = new List<List<int>>();
            foreach (string s in lineStructure)
            {
                s.Trim();
                List<int> charsToNumbers = new List<int>();
                foreach (char c in s)
                {
                    if (c == ' ')
                        charsToNumbers.Add(1);
                    else
                        charsToNumbers.Add(0);
                }
                mapOfRivers.Add(charsToNumbers);
            }
            // Line lengths will help to not get index out of range exception:
            List<int> lineLengths = new List<int>();
            foreach (List<int> map in mapOfSpaces)
            {
                lineLengths.Add(map.Count);
            }
            // Now we calculate the river lengths with the following foreach loop:
            bool firstIteration = true;
            int index = 0;
            foreach (List<int> map in mapOfSpaces)
            {
                // We skip the first iteration because there is no line above it to compare to.
                if (!firstIteration)
                {
                    // Index the row above (one less than index) where index represents the line below.
                    int rowAbove = index - 1;
                    // charDifference tells us which line is larger, (above or below).
                    int charDifference = lineLengths[index] - lineLengths[rowAbove];
                    // CASE:
                    if (charDifference < 0) // Above Line is larger
                    {
                        bool firstMapIteration = true;
                        // Since above line is larger we can check in three directions for the whole length of the line
                        // (length of line is map.Count) provided we skip the first character since it will not yield a
                        // river extension.
                        for (int i = 0; i < map.Count; i++)
                        {
                            // Set a bool to disallow counting a river extension twice.
                            bool spaceCounted = false;
                            // Skip first character on the line. Then:
                            if (!firstMapIteration)
                            {
                                // Check above, above-left, above-right
                                if (mapOfSpaces[index][i] + mapOfSpaces[rowAbove][i] == 2) // above
                                {
                                    mapOfRivers[index][i] = mapOfRivers[index][i] + mapOfRivers[rowAbove][i];
                                    spaceCounted = true;
                                }
                                if (mapOfSpaces[index][i] + mapOfSpaces[rowAbove][i + 1] == 2 && !spaceCounted) //above-right
                                {
                                    mapOfRivers[index][i] = mapOfRivers[index][i] + mapOfRivers[rowAbove][i + 1];
                                    spaceCounted = true;
                                }

                                if (mapOfSpaces[index][i] + mapOfSpaces[rowAbove][i - 1] == 2 && !spaceCounted) //above-left
                                    mapOfRivers[index][i] = mapOfRivers[index][i] + mapOfRivers[rowAbove][i - 1];
                            }
                            firstMapIteration = false;
                        }
                    }
                    // CASE:
                    if (charDifference >= 0) // Above line is shorter or equal
                    {
                        // Since the above line will be shorter or equal, we iterate to one character less than the length
                        // of the row above, checking for river extensions above, above-left and above-right.
                        bool firstMapIteration = true;
                        for (int i = 0; i < mapOfSpaces[rowAbove].Count - 1; i++) // The difference here is range.
                        {
                            // Set a bool to disallow counting a river extension twice.
                            bool spaceCounted = false;
                            // Again skip the first character because it will not yield river extensions.
                            if (!firstMapIteration)
                            {
                                // Check above, above-left, above-right
                                if (mapOfSpaces[index][i] + mapOfSpaces[rowAbove][i] == 2) // above
                                {
                                    mapOfRivers[index][i] = mapOfRivers[index][i] + mapOfRivers[rowAbove][i];
                                    spaceCounted = true;
                                }
                                if (mapOfSpaces[index][i] + mapOfSpaces[rowAbove][i + 1] == 2 && !spaceCounted) // above-right
                                {
                                    mapOfRivers[index][i] = mapOfRivers[index][i] + mapOfRivers[rowAbove][i + 1];
                                    spaceCounted = true;
                                }
                                if (mapOfSpaces[index][i] + mapOfSpaces[rowAbove][i - 1] == 2 && !spaceCounted) // above-left
                                    mapOfRivers[index][i] = mapOfRivers[index][i] + mapOfRivers[rowAbove][i - 1];
                                // If you are on the last iteration, do this addtional check, above-left.
                                // Because the line below is larger than the line above it may produce a river extension.
                                // Directly above is not checked because the last character on the line above will be a letter
                                // which does not yield a river extension.
                                if (i == mapOfSpaces[rowAbove].Count - 2)
                                {
                                    int extraPos = mapOfSpaces[rowAbove].Count - 1;
                                    if (mapOfSpaces[index][extraPos] + mapOfSpaces[rowAbove][i - 1] == 2)
                                        mapOfRivers[index][extraPos] = mapOfRivers[index][extraPos] + 
                                            mapOfRivers[rowAbove][i - 1];
                                }
                            }
                            firstMapIteration = false;
                        }
                    }
                }
                firstIteration = false;
                index++;
            }
            // Now we extract the maximum river length from the map and return it.
            int maxRiver = 0;
            foreach (List<int> rivers in mapOfRivers)
            {
                foreach (int i in rivers)
                {
                    if (i >= maxRiver)
                        maxRiver = i;
                }
            }
            return maxRiver;
        }
        #endregion

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void changed([CallerMemberName] string property = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion
    }
}
