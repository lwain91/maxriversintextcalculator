﻿namespace RiverCalculator
{
    public class LayoutData
    {
        #region Properties
        public int IterationID { get; set; }
        public int LineWidth { get; set; }
        public int MaxRiver { get; set; }
        #endregion
    }
}
