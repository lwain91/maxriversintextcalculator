﻿using System.Collections.Generic;

namespace RiverCalculator
{
    public class Page
    {
        #region Properties

        // Initial Properties
        public int WordCount { get; set; }

        public List<string> RawText { get; set; }      

        public int LineWidth { get; set; }

        // Properties to be defined after GetFileData() is called        

        public int LongestRiver { get; set; }
        #endregion        
    }
}
