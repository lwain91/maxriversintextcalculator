Installation Instructions:

1. Clone the project to a directory
2. Build using visual studio 2019

Use:

Follow the instructions in the program.

*** Note on License ***

This software has an MIT License. This was chosen because anyone should feel free to use, copy, modify, merge, publish, 
distribute, sublicense, and/or sell copies of the Software free of charge.